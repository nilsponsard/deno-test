let last_id = 0
let messages = document.getElementById("messages")
let send_message = document.getElementById("send_message")
let input_text = document.getElementById("input_text")
let input_username = document.getElementById("input_username")
let form_message = document.getElementById("form_message")

function load() {
    let request = new XMLHttpRequest()
    // bake the request
    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                let parsed = JSON.parse(this.responseText)
                for (let index in parsed) {
                    let message = parsed[index]
                    if (message.id > last_id) {
                        let message_div = document.createElement("div")
                        message_div.innerText = message.username + " : " + message.text
                        last_id = message.id
                        messages.appendChild(message_div)
                    }
                }

            } else {
                console.error(this.responseText)
            }
            setTimeout(load, 1000)
        }
    }
    request.open("get", "api/message/after_id/" + last_id.toString(), true)
    request.send()
}


function init() {
    load()
}

init()

function send() {

    let request = new XMLHttpRequest()
    // bake the request
    request.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                console.log("message sent")
            } else {
                console.error(this.responseText)
            }
            setTimeout(load, 100)
        }
    }
    request.open("post", "api/message/send/" + input_username.value, true)
    // request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
    request.send(input_text.value)
    input_text.value = ""
}

send_message.addEventListener("click", send)
form_message.addEventListener("keyup", ev => {
    if (ev.key === "Enter") {

        if (!ev.ctrlKey) {

            ev.preventDefault()
            send()
        }

    }
})