import {Application} from "https://deno.land/x/oak/mod.ts";
import * as api from "./api.ts"

const app = new Application();

app.use(async (ctx) => {
    const path = ctx.request.url.pathname
    if (path.startsWith("/api")) {
        api.parse(ctx)
    } else {
        await ctx.send({
            root: `${Deno.cwd()}/public`,
            index: "index.html",
        })
    }
});

app.listen("0.0.0.0:80");
console.log("http://127.0.0.1:8000")
