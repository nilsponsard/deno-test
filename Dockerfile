FROM debian:latest
RUN apt-get update && apt-get install curl unzip -y
RUN curl -fsSL https://deno.land/x/install/install.sh | sh && mv /root/.deno/bin/deno /bin/deno
COPY ./ /app/
WORKDIR /app/
ENTRYPOINT ["deno"]
CMD ["run", "-A", "/app/index.ts"]