import {Context} from "https://deno.land/x/oak/mod.ts";
import * as messages from "./Message.ts"


export function parse(ctx: Context<Record<string, any>>) {
    if (ctx.request.method == "GET") {
        if (ctx.request.url.pathname.startsWith("/api/message/after_id/")) {
            let id = parseInt(ctx.request.url.pathname.replace("/api/message/after_id/", ""))
            let out = [] as Array<messages.Message>
            if (id >= 0) {
                for (let i = id; i < messages.messages.length; ++i) {
                    out.push(messages.messages[i])
                }
                ctx.response.body = JSON.stringify(out)
            }
        }
    } else if (ctx.request.method == "POST") {
        if (ctx.request.url.pathname.startsWith("/api/message/send/")) {
            let username = ctx.request.url.pathname.replace("/api/message/send/", "")
            let body = ctx.request.body({type: "text"})
            body.value.then((value) => {
                messages.messages.push(new messages.Message(username, value))
            })
            ctx.response.status = 200
        }
    }
}
