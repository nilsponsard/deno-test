export let last_id = 0

export let messages = [] as Array<Message>

export class Message {
    id: number
    username: string
    text: string

    constructor(username: string, text: string) {
        this.id = ++last_id
        this.username = username
        this.text = text
    }
}